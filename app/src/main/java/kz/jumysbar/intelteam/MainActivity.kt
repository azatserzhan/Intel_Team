package kz.jumysbar.intelteam

import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.net.toUri
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.preference.PreferenceManager
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.header_main_menu.*
import kotlinx.android.synthetic.main.personal_page_fragment.*
import kz.jumysbar.intelteam.model.User
import kz.jumysbar.intelteam.ui.tour.fragment.CreateNewTour
import kz.jumysbar.intelteam.ui.tour.fragment.HeaderMenuInterface

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    HeaderMenuInterface {
    private var navController: NavController? = null
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var appBarConfiguration: AppBarConfiguration
    private var googleSignInClient: GoogleSignInClient? = null
    private val db = Firebase.database
    private var firebaseAuth: FirebaseAuth? = null
    private var prefs: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        drawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        navController = findNavController(R.id.nav_host_fragment)
        //   navView.setNavigationItemSelectedListener(this) drawer_layout.closeDrawers()
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_authorization, R.id.nav_search, R.id.nav_tour,
                R.id.nav_tools, R.id.nav_useful_materials,
                R.id.nav_calendar, R.id.nav_weather,
                R.id.nav_history, R.id.nav_location,
                R.id.nav_settings, R.id.nav_exit,
                R.id.nav_compass, R.id.nav_create_tour, R.id.nav_userTours, R.id.nav_chat

            ), drawerLayout
        )
        setupActionBarWithNavController(navController!!, appBarConfiguration)
        navView.setupWithNavController(navController!!)
        fab.setOnClickListener {
            sendEmail()
        }
        prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        val prefsUserId = prefs?.getString("googleUserID", "").orEmpty()
        setUserNameImgHeader(prefsUserId)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    fun exitUser(menuItem: MenuItem) {
        firebaseAuth = FirebaseAuth.getInstance()
        val options = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(this.getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(applicationContext, options)
        googleSignInClient?.signOut()
        firebaseAuth?.signOut()
        val prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        prefs.edit().apply {
            putBoolean("googleUserSigned", false)
            remove("googleUserID")
            clear()
        }.apply()
        setUserNameImgHeader("")
        Snackbar.make(drawer_layout, getString(R.string.snackbar_exit_auth), 2000).show()
    }

    private fun sendEmail() {
        val email = Intent(Intent.ACTION_SENDTO)
        email.data = Uri.parse("mailto:jumysbar@gmail.com")
        email.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject))
        email.putExtra(Intent.EXTRA_TEXT, getString(R.string.email_text))
        startActivity(email)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.nav_create_tour -> supportFragmentManager.beginTransaction()
                .replace(R.id.nav_host_fragment, CreateNewTour())
                .commit()
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun setUserNameImgHeader(userID: String) {
        if (userID.isNotEmpty()) {
            val databaseReference = userID.let { db.getReference("users").child(it) }
            databaseReference.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val username = snapshot.getValue(User::class.java)?.username.toString()
                    val avatarUrl = snapshot.getValue(User::class.java)?.userPhotoURL?.toUri()
                    val navHeaderView = nav_view?.getHeaderView(0)
                    val tvHeaderName =
                        navHeaderView?.findViewById<TextView>(R.id.txtV_name_user_header_main)
                    val imgHeader =
                        navHeaderView?.findViewById<ImageView>(R.id.imgV_user_header_main)
                    tvHeaderName?.text = username

                    imgHeader?.let {
                        Picasso.get()
                            .load(avatarUrl).fit().centerCrop().into(it)
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    Log.d("onCancelled", error.message)
                }
            })
        } else {
            val navHeaderView = nav_view?.getHeaderView(0)
            val tvHeaderName =
                navHeaderView?.findViewById<TextView>(R.id.txtV_name_user_header_main)
            val imgHeader = navHeaderView?.findViewById<ImageView>(R.id.imgV_user_header_main)
            tvHeaderName?.text = "UserName"
            imgHeader?.let {
                Glide.with(this@MainActivity)
                    .load(R.drawable.ic_perm_identity).into(it)
            }
        }
    }
}
