package kz.jumysbar.intelteam.ui.authorization

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.authorization_fragment.*
import kotlinx.android.synthetic.main.authorization_fragment.signInBtn
import kotlinx.android.synthetic.main.authorization_fragment_email.*
import kz.jumysbar.intelteam.R
import kz.jumysbar.intelteam.fragment.PersonalPageFragment
import kz.jumysbar.intelteam.model.User
import kz.jumysbar.intelteam.ui.tour.util.Utils

private const val GOOGLE_SIGN_IN = 1005

class AuthorizationFragment : Fragment() {
    private var googleSignInClient: GoogleSignInClient? = null
    private var firebaseAuth: FirebaseAuth? = null
    private var compositeDisposable = CompositeDisposable()
    private lateinit var prefs: SharedPreferences
    private val db = Firebase.database
    private val auth = Firebase.auth

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.authorization_fragment_email, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prefs = PreferenceManager.getDefaultSharedPreferences(context?.applicationContext)
        val isSigned = prefs.getBoolean("googleUserSigned", false)
        if (isSigned) {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.auth_container, PersonalPageFragment())
                ?.addToBackStack(PersonalPageFragment::class.simpleName)
                ?.commit()
        } else {
            signInBtn?.let { setGooglePlusButtonText(it, getString(R.string.login_with_google)) }
            signInBtn?.setOnClickListener {
                signIn()
            }
            loginButton.setOnClickListener { loginUser() }
            createButton.setOnClickListener { createUser() }
        }
    }

    private fun signIn() {
        val disposable = Completable.fromAction {
            progress_circleAuth?.progress = View.VISIBLE
            firebaseAuth = FirebaseAuth.getInstance()
            val options = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(this.getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
            googleSignInClient = GoogleSignIn.getClient(requireContext(), options)
            val signInIntent = googleSignInClient?.signInIntent
            startActivityForResult(signInIntent, GOOGLE_SIGN_IN)
            progress_circleAuth?.progress = View.INVISIBLE
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.d("App", "signIn:OK ")
            }, {
                Log.e("App", "signIn: $it ")
            })
        compositeDisposable.add(disposable)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (Utils.isNetworkAvailable(requireContext())) {
            if (requestCode == GOOGLE_SIGN_IN) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    val account = task.getResult(ApiException::class.java)
                    val credential = GoogleAuthProvider.getCredential(account?.idToken, null)
                    firebaseAuth?.signInWithCredential(credential)
                        ?.addOnCompleteListener { authResult ->
                            if (authResult.isSuccessful) {
                                firebaseAuth?.currentUser?.let { user ->
                                    user.getIdToken(true).addOnCompleteListener { result ->
                                        result.result?.token
                                        db.getReference("users").child(user.uid)
                                            .addValueEventListener(object : ValueEventListener {
                                                override fun onDataChange(snapshot: DataSnapshot) {
                                                    val userApp = User(
                                                        user.displayName,
                                                        user.email,
                                                        user.photoUrl?.toString(),
                                                        snapshot.getValue(User::class.java)?.userAge
                                                            ?: 0,
                                                        snapshot.getValue(User::class.java)?.userSex
                                                            ?: "Не указан",
                                                        snapshot.getValue(User::class.java)?.userWork
                                                            ?: "Не указан",
                                                        snapshot.getValue(User::class.java)?.userLevel
                                                            ?: "Не указан",
                                                        snapshot.getValue(User::class.java)?.userBall
                                                            ?: 0
                                                    )
                                                    db.getReference("users").child(user.uid)
                                                        .setValue(userApp)
                                                    progress_circleAuth?.progress = View.INVISIBLE
                                                }

                                                override fun onCancelled(error: DatabaseError) {
                                                    Log.e("error", "onCancelled: $error.message ")
                                                    progress_circleAuth?.progress = View.INVISIBLE
                                                }
                                            })
                                        prefs =
                                            PreferenceManager.getDefaultSharedPreferences(
                                                requireContext()
                                            )
                                        prefs.edit()?.putBoolean("googleUserSigned", true)?.apply()
                                        prefs.edit()?.putString("googleUserID", user.uid)?.apply()
                                    }
                                    activity?.supportFragmentManager
                                        ?.beginTransaction()
                                        ?.replace(
                                            R.id.auth_container,
                                            PersonalPageFragment.newInstance(user.uid)
                                        )?.commit()
                                }
                            } else {
                                Log.e(
                                    "SigIn",
                                    "Failure signInWithCredential ",
                                    authResult.exception
                                )
                            }
                        }

                } catch (e: ApiException) {
                    view?.let { Snackbar.make(it, "Google sign in failed", 2500).show() }
                }
            }
        } else {
            view?.let { Snackbar.make(it, "Нет подключения к интернету!", 2500).show() }
        }
    }

    private fun setGooglePlusButtonText(signInBtn: SignInButton?, btnTxt: String) {
        for (i in 0 until signInBtn?.childCount!!) {
            val v: View = signInBtn.getChildAt(i)
            if (v is TextView) {
                v.text = btnTxt
                return
            }
        }
    }

    private fun checkUser() {
        progress_circleAuthEmail?.visibility = View.VISIBLE
        val currentUser = auth.currentUser
        if (currentUser != null) {
            currentUser.let { user ->
                user.getIdToken(true).addOnCompleteListener { result ->
                    result.result?.token
                    db.getReference("users").child(user.uid)
                        .addValueEventListener(object : ValueEventListener {
                            override fun onDataChange(snapshot: DataSnapshot) {
                                val userApp = User(
                                    user.displayName ?: "Неизвестный",
                                    user.email,
                                    user.photoUrl?.toString(),
                                    snapshot.getValue(User::class.java)?.userAge ?: 0,
                                    snapshot.getValue(User::class.java)?.userSex ?: "Не указан",
                                    snapshot.getValue(User::class.java)?.userWork ?: "Не указан",
                                    snapshot.getValue(User::class.java)?.userLevel ?: "Новичок",
                                    snapshot.getValue(User::class.java)?.userBall ?: 0
                                )

                                db.getReference("users").child(user.uid)
                                    .setValue(userApp)
                                progress_circleAuthEmail?.visibility = View.INVISIBLE
                            }

                            override fun onCancelled(error: DatabaseError) {
                                Log.e("error", "onCancelled: $error.message ")
                            }
                        })
                    prefs =
                        PreferenceManager.getDefaultSharedPreferences(requireContext())
                    prefs.edit()?.putBoolean("googleUserSigned", true)?.apply()
                    prefs.edit()?.putString("googleUserID", user.uid)?.apply()
                }
                activity?.supportFragmentManager
                    ?.beginTransaction()
                    ?.replace(
                        R.id.auth_container,
                        PersonalPageFragment.newInstance(user.uid)
                    )?.commit()
            }
        } else {
            view?.let { Snackbar.make(it, "Google sign in failed", 2500).show() }
        }

    }

    private fun createUser() {
        val email = emailText.text.toString()
        val password = passwordText.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty()) {
            auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    checkUser()
                } else {
                    task.exception.let {
                        view?.let { Snackbar.make(it, "Google sign in failed", 2500).show() }
                    }
                }
            }
        } else {
            view?.let { Snackbar.make(it, "Введите корректные данные!", 2500).show() }
        }
    }

    private fun loginUser() {
        val email = emailText.text.toString()
        val password = passwordText.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty()) {
            auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    checkUser()
                } else {
                    task.exception.let {
                        view?.let { Snackbar.make(it, "Google account is Failed", 2500).show() }
                    }
                }
            }
        } else {
            view?.let { Snackbar.make(it, "Введите корректные данные!", 2500).show() }
        }


    }
}

