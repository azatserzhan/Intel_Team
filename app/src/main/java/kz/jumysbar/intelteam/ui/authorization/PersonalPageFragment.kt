package kz.jumysbar.intelteam.fragment

import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.View
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.header_main_menu.*
import kotlinx.android.synthetic.main.personal_page_fragment.*
import kotlinx.android.synthetic.main.user_tours_fragment.*
import kz.jumysbar.intelteam.R
import kz.jumysbar.intelteam.model.User
import kz.jumysbar.intelteam.ui.tour.fragment.HeaderMenuInterface
import kz.jumysbar.intelteam.ui.tour.util.Utils

class PersonalPageFragment : Fragment(R.layout.personal_page_fragment) {
    private lateinit var prefs: SharedPreferences
    private var compositeDisposable = CompositeDisposable()
    private val db = Firebase.database
    private lateinit var userId: String

    companion object {
        fun newInstance(userId: String): PersonalPageFragment {
            val args = Bundle()
            args.putString("googleUId", userId)
            val fragment = PersonalPageFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prefs = PreferenceManager.getDefaultSharedPreferences(requireContext())
        val prefsUserId = prefs.getString("googleUserID", "").orEmpty()
        userId = if (prefsUserId.isEmpty()) {
            arguments?.getString("googleUId", "").orEmpty()
        } else {
            prefsUserId
        }
        recognizeUser(userId)
       fixedButtonsClick()

        saveFixesBtn?.setOnClickListener {
            if (ageEdTxt?.text?.isNotEmpty() == true && sexEdTxt?.text?.isNotEmpty() == true && userWorkEdTxt?.text?.isNotEmpty() == true) {
                val disposable = Completable.fromAction {
                    db.getReference("users")
                        .child(userId).apply {
                            child("userAge").setValue(ageEdTxt?.text.toString().trim().toInt())
                            child("userSex").setValue(sexEdTxt?.text.toString().trim())
                            child("userWork").setValue(userWorkEdTxt?.text.toString().trim())
                        }
                }.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        {
                            Snackbar.make(view, "Сохранены!", 2500).show()
                        },
                        {
                            Log.e("App", "onViewCreated: ", it)
                        }
                    )
                compositeDisposable.add(disposable)
                ageEdTxt?.isEnabled = false
                sexEdTxt?.isEnabled = false
                userWorkEdTxt?.isEnabled = false
            }else{
                Snackbar.make(view, "Пустое поле!Заполните! ", 2500).show()
            }
        }
    }

    private fun fixedButtonsClick(){
        fixedAgeBtn?.setOnClickListener {
            ageEdTxt?.isEnabled = true

        }
        fixedSexBtn?.setOnClickListener {
            sexEdTxt?.isEnabled = true
        }
        fixedWorkBtn?.setOnClickListener {
            userWorkEdTxt?.isEnabled = true
        }
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.dispose()
    }

    private fun recognizeUser(userId: String) {
        val id = userId
        val ref = db.getReference("users").child(id)
        if (Utils.isNetworkAvailable(requireContext())) {
            val disposable = Completable.fromAction {
                ref.addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        userName?.text = Editable.Factory
                            .getInstance().newEditable(snapshot.getValue(User::class.java)?.username.toString())
                        userEmail?.text = snapshot.getValue(User::class.java)?.email.toString()
                        userAvatar?.let {
                            Glide.with(this@PersonalPageFragment)
                                .load(snapshot.getValue(User::class.java)?.userPhotoURL?.toUri())
                                .into(it)
                        }
                        ageEdTxt?.text = Editable.Factory.getInstance()
                            .newEditable(snapshot.getValue(User::class.java)?.userAge.toString())
                        sexEdTxt?.text = Editable.Factory.getInstance()
                            .newEditable(snapshot.getValue(User::class.java)?.userSex.toString())
                        userWorkEdTxt?.text = Editable.Factory.getInstance()
                            .newEditable(snapshot.getValue(User::class.java)?.userWork.toString())
                        userLevelEdTxt?.text = Editable.Factory.getInstance()
                            .newEditable(snapshot.getValue(User::class.java)?.userLevel.toString())
                        userBAllEdTxt?.text = Editable.Factory.getInstance()
                            .newEditable(snapshot.getValue(User::class.java)?.userBall.toString())
                        (activity as HeaderMenuInterface).setUserNameImgHeader(id)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Log.d("onCancelled", error.message)
                    }
                })
            }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.d("App", "recognizeUser: success!")
                }, {
                    Log.e("App", "recognizeUser: ", it)
                })
            compositeDisposable.add(disposable)
        } else {
            view?.let { Snackbar.make(it, "Нет подключения к интернету!", 2500).show() }
        }
    }
}