package kz.jumysbar.intelteam.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class User(
    var username: String? = "",
    var email: String? = "",
    var userPhotoURL: String? = "",
    var userAge: Int? = 0,
    var userSex: String? = "",
    var userWork: String? = "",
    var userLevel: String? = "",
    var userBall: Int? = 0
)