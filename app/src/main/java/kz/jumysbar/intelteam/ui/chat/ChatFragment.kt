package kz.jumysbar.intelteam.ui.chat

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import kotlinx.android.synthetic.main.fragment_chat.*
import kz.jumysbar.intelteam.R

class ChatFragment : Fragment(R.layout.fragment_chat) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        constraintLayout.setOnClickListener {
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            transaction.replace(R.id.nav_host_fragment, Messages())
            transaction.disallowAddToBackStack()
            transaction.commit()
        }
    }
}