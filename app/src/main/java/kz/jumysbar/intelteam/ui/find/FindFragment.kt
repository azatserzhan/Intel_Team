package kz.jumysbar.intelteam.ui.find

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_find.recyclerViewSearch
import kz.jumysbar.intelteam.R

class FindFragment : Fragment(), PostsAdapter.OnItemClickListener {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_find, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val posts = listOf<ItemSearch>(
            ItemSearch("Мальдивы", R.drawable.maldives),
            ItemSearch("Китай", R.drawable.china),
            ItemSearch("Бразилия", R.drawable.brazil),
            ItemSearch("Россия", R.drawable.russia)
        )

        recyclerViewSearch.apply {
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            setHasFixedSize(true)
            adapter =
                activity?.applicationContext?.let { PostsAdapter(it, posts, this@FindFragment) }

        }
    }

    override fun onItemClick(position: Int) {
        Toast.makeText(activity, "Item $position clicked", Toast.LENGTH_SHORT).show()

    }
}