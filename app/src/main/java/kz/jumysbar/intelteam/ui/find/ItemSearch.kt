package kz.jumysbar.intelteam.ui.find

data class ItemSearch(
    var titleForSuggestions: String,
    var imgForSuggestions: Int
)