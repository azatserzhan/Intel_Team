package kz.jumysbar.intelteam.ui.find

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.search_item.view.imageSuggestions
import kotlinx.android.synthetic.main.search_item.view.txtSuggestions
import kz.jumysbar.intelteam.R

class PostsAdapter(
    private val context: Context,
    private val images: List<ItemSearch>,
    private val listener: OnItemClickListener,
    private var lastPosition: Int = -1
) : RecyclerView.Adapter<PostsAdapter.ImageViewHolder>() {

    inner class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private val img = itemView.imageSuggestions
        private val txt = itemView.txtSuggestions
        fun bindView(image: ItemSearch) {
            img.setImageResource(image.imgForSuggestions)
            txt.text = image.titleForSuggestions
        }

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position)
            }
        }
    }

    interface OnItemClickListener {

        fun onItemClick(position: Int)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder =
        ImageViewHolder(LayoutInflater.from(context).inflate(R.layout.search_item, parent, false))

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(images[position])
        setAnimation(holder.itemView, position)
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation: Animation =
                AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }
}