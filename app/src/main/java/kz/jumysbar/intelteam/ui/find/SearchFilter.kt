package kz.jumysbar.intelteam.ui.find

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.database_tour.valueSearch
import kotlinx.android.synthetic.main.database_tour.plusBtnSearch
import kotlinx.android.synthetic.main.database_tour.minusBtnSearch
import kz.jumysbar.intelteam.R

class SearchFilter : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_filter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var id = 0
        valueSearch.setText("" + id)

        plusBtnSearch.setOnClickListener {
            valueSearch.setText("" + ++id)
        }

        minusBtnSearch.setOnClickListener {
            valueSearch.setText("" + --id)
        }
    }
}