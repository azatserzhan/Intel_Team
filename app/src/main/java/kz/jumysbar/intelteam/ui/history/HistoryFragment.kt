package kz.jumysbar.intelteam.ui.history

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.fragment_history.recycler_view_history
import kz.jumysbar.intelteam.R

class HistoryFragment : Fragment(R.layout.fragment_history) {
    private var gridLayoutManager: GridLayoutManager? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val imgsForHistory = listOf<imageForHistory>(
            imageForHistory("Лас-Вегас", 5, R.drawable.lasvegas, 1250000),
            imageForHistory("Сеул", 4, R.drawable.south_korea, 780000),
            imageForHistory("Сидней", 3, R.drawable.sydney, 907000),
            imageForHistory("Аскона", 5, R.drawable.switzerland, 1367000),
            imageForHistory("Танзания", 5, R.drawable.safari, 538000),
            imageForHistory("Сочи", 4, R.drawable.sochi, 907000),
        )

        recycler_view_history.apply {
            layoutManager = GridLayoutManager(activity, 2)

            adapter = activity?.applicationContext?.let { RecyclerViewHistory(it, imgsForHistory) }
        }
    }
}