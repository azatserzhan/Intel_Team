package kz.jumysbar.intelteam.ui.history

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_history.view.icon_image_history
import kotlinx.android.synthetic.main.item_history.view.title_text_history
import kotlinx.android.synthetic.main.item_history.view.ratingOfPlace
import kotlinx.android.synthetic.main.item_history.view.price_history
import kz.jumysbar.intelteam.R

class RecyclerViewHistory(
    private val context: Context,
    private val imagesHstr: List<imageForHistory>,
    private var lastPosition: Int = -1
) : RecyclerView.Adapter<RecyclerViewHistory.ImageViewHolder>() {

    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val img = itemView.icon_image_history
        val place = itemView.title_text_history
        val rating = itemView.ratingOfPlace
        val price = itemView.price_history
        fun bindView(image: imageForHistory) {
            img.setImageResource(image.imgHistory)
            place.text = image.placeHistory
            rating.text = "Рейтинг: " + image.rating.toString()
            price.text = "Стоимость: " + image.price.toString() + " KZT"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder =
        ImageViewHolder(LayoutInflater.from(context).inflate(R.layout.item_history, parent, false))

    override fun getItemCount(): Int = imagesHstr.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(imagesHstr[position])
        setAnimation(holder.itemView, position)
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation: Animation =
                AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }
}