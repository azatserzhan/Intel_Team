package kz.jumysbar.intelteam.ui.tools

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import kotlinx.android.synthetic.main.tools_food.imageButtonFood
import kz.jumysbar.intelteam.R


class ToolsFood : Fragment(R.layout.tools_food) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imageButtonFood.setOnClickListener {
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(android.R.anim.fade_out, android.R.anim.slide_out_right)
            transaction.replace(R.id.nav_host_fragment, ToolsFragment())
            transaction.disallowAddToBackStack()
            transaction.commit()
        }
    }
}