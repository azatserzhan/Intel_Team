package kz.jumysbar.intelteam.ui.tools

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_tools.clothes_btn
import kotlinx.android.synthetic.main.fragment_tools.food_btn
import kotlinx.android.synthetic.main.fragment_tools.tech_btn
import kotlinx.android.synthetic.main.fragment_tools.hygiene_btn
import kotlinx.android.synthetic.main.fragment_tools.medicine_btn
import kotlinx.android.synthetic.main.fragment_tools.other_btn
import kz.jumysbar.intelteam.R

class ToolsFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //  Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tools, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clothes_btn.setOnClickListener {
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            transaction.replace(R.id.nav_host_fragment, toolsClothes())
            transaction.disallowAddToBackStack()
            transaction.commit()
        }
        food_btn.setOnClickListener {
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            transaction.replace(R.id.nav_host_fragment, ToolsFood())
            transaction.disallowAddToBackStack()
            transaction.commit()
        }
        tech_btn.setOnClickListener {
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            transaction.replace(R.id.nav_host_fragment, ToolsGadjets())
            transaction.disallowAddToBackStack()
            transaction.commit()
        }
        hygiene_btn.setOnClickListener {
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            transaction.replace(R.id.nav_host_fragment, ToolsHygiene())
            transaction.disallowAddToBackStack()
            transaction.commit()
        }
        medicine_btn.setOnClickListener {
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            transaction.replace(R.id.nav_host_fragment, ToolsMedicine())
            transaction.disallowAddToBackStack()
            transaction.commit()
        }
        other_btn.setOnClickListener {
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            transaction.replace(R.id.nav_host_fragment, ToolsOther())
            transaction.disallowAddToBackStack()
            transaction.commit()
        }
    }
}