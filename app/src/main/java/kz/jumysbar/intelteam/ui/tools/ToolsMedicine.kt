package kz.jumysbar.intelteam.ui.tools

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import kotlinx.android.synthetic.main.tools_medicine.imageButtonMedicine
import kz.jumysbar.intelteam.R

class ToolsMedicine : Fragment(R.layout.tools_medicine) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imageButtonMedicine.setOnClickListener {
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(android.R.anim.fade_out, android.R.anim.slide_out_right)
            transaction.replace(R.id.nav_host_fragment, ToolsFragment())
            transaction.disallowAddToBackStack()
            transaction.commit()
        }
    }
}