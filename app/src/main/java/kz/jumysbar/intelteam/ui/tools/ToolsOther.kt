package kz.jumysbar.intelteam.ui.tools

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import kotlinx.android.synthetic.main.tools_other.imageButtonOther
import kz.jumysbar.intelteam.R

class ToolsOther : Fragment(R.layout.tools_other) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        imageButtonOther.setOnClickListener {
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(android.R.anim.fade_out, android.R.anim.slide_in_left)
            transaction.replace(R.id.nav_host_fragment, ToolsFragment())
            transaction.disallowAddToBackStack()
            transaction.commit()
        }
    }
}