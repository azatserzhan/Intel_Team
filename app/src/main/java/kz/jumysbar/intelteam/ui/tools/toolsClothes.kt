package kz.jumysbar.intelteam.ui.tools

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.tools_clothes.imageButtonClothes
import kz.jumysbar.intelteam.R

class toolsClothes : Fragment(R.layout.tools_clothes) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imageButtonClothes.setOnClickListener {
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(android.R.anim.fade_out, android.R.anim.slide_in_left)
            transaction.replace(R.id.nav_host_fragment, ToolsFragment())
            transaction.disallowAddToBackStack()
            transaction.commit()
        }
    }
}