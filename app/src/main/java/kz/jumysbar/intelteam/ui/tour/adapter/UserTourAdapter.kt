package kz.jumysbar.intelteam.ui.tour.adapter

import android.content.Context
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.fragment.app.findFragment
import androidx.navigation.findNavController
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.user_tour_item.view.nameTour
import kotlinx.android.synthetic.main.user_tour_item.view.img_view_tour
import kotlinx.android.synthetic.main.user_tour_item.view.tourCountry
import kotlinx.android.synthetic.main.user_tour_item.view.tourPrice
import kotlinx.android.synthetic.main.user_tour_item.view.tourPeriod
import kotlinx.android.synthetic.main.user_tour_item.view.tourDesc
import kz.jumysbar.intelteam.R
import kz.jumysbar.intelteam.ui.tour.fragment.TourFragment
import kz.jumysbar.intelteam.ui.tour.model.NewTourItem

class UserTourAdapter(
) : RecyclerView.Adapter<UserTourAdapter.ImageViewHolder>() {

    var mListener: itemClickListener? = null
    private var tourList = mutableListOf<NewTourItem>()
    private var mContext: Context? = null

    constructor(context: Context, uploads: MutableList<NewTourItem>) : this() {
        mContext = context
        tourList = uploads
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val v: View = LayoutInflater.from(mContext).inflate(R.layout.user_tour_item, parent, false)
        return ImageViewHolder(v)
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val item = tourList[position]
        holder.name.text = item.tourName
        Picasso.get().load(item.tourImgUrl?.toUri())
            .placeholder(R.mipmap.ic_launcher).fit()
            .centerCrop().into(holder.imageView)

        holder.period.text = item.tourPeriod
        holder.description.text = item.tourDesc
        holder.country.text = item.tourCountry + ", " + item.tourCity
        holder.price.text = item.tourPrice.toString() + " kzt"

        holder.itemView.setOnClickListener { view ->
            val prefs = PreferenceManager.getDefaultSharedPreferences(mContext)
            prefs?.edit()?.putString("tourKey", item.mKey)?.apply()

            if (view.findFragment<Fragment>() is TourFragment) {
                view.findNavController()
                    .navigate(R.id.action_navigation_tourFragment_to_tourDetailFragment)
            } else {
                view.findNavController()
                    .navigate(R.id.action_nav_userTours_to_tourDetailFragment)
            }

        }
    }

    override fun getItemCount(): Int = tourList.size

    inner class ImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnCreateContextMenuListener, MenuItem.OnMenuItemClickListener {
        var name: TextView = itemView.nameTour
        var imageView: ImageView = itemView.img_view_tour
        var country: TextView = itemView.tourCountry
        var price: TextView = itemView.tourPrice
        var period: TextView = itemView.tourPeriod
        var description: TextView = itemView.tourDesc

        init {
            itemView.setOnCreateContextMenuListener(this)
        }


        override fun onCreateContextMenu(
            menu: ContextMenu?,
            v: View?,
            menuInfo: ContextMenu.ContextMenuInfo?
        ) {
            val delete: MenuItem = menu!!.add(Menu.NONE, 1, 1, "Удалить")
            delete.setOnMenuItemClickListener(this)

        }

        override fun onMenuItemClick(item: MenuItem): Boolean {
            if (mListener != null) {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    when (item.itemId) {
                        1 -> {
                            mListener?.onDeleteClick(position)
                            return true
                        }
                    }
                }
            }
            return false
        }
    }

    interface itemClickListener {
        // fun onItemClick(pos: Int)
        fun onDeleteClick(pos: Int)
    }

    fun setOnItemClickListener(listener: itemClickListener) {
        mListener = listener
    }
}