package kz.jumysbar.intelteam.ui.tour.fragment

import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.content.ContentResolver
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.StorageTask
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.create_new_tour_fragment.btn_choose_img
import kotlinx.android.synthetic.main.create_new_tour_fragment.addTourBtn

import kotlinx.android.synthetic.main.create_new_tour_fragment.input_tour_name
import kotlinx.android.synthetic.main.create_new_tour_fragment.input_tour_desc
import kotlinx.android.synthetic.main.create_new_tour_fragment.input_tour_city
import kotlinx.android.synthetic.main.create_new_tour_fragment.input_tour_country
import kotlinx.android.synthetic.main.create_new_tour_fragment.input_tour_price
import kotlinx.android.synthetic.main.create_new_tour_fragment.input_flight_date
import kotlinx.android.synthetic.main.create_new_tour_fragment.input_tour_path
import kotlinx.android.synthetic.main.create_new_tour_fragment.input_tour_players
import kotlinx.android.synthetic.main.create_new_tour_fragment.input_arrival
import kotlinx.android.synthetic.main.create_new_tour_fragment.img_view
import kotlinx.android.synthetic.main.create_new_tour_fragment.showAdds
import kotlinx.android.synthetic.main.create_new_tour_fragment.scrollViewLayout
import kotlinx.android.synthetic.main.create_new_tour_fragment.warningLayout
import kotlinx.android.synthetic.main.create_new_tour_fragment.progress_bar
import kz.jumysbar.intelteam.R
import kz.jumysbar.intelteam.ui.tour.model.NewTourItem
import java.util.Calendar

private const val PICK_IMG_REQUEST = 1

class CreateNewTour : Fragment(R.layout.create_new_tour_fragment) {
    private var mImgURi: Uri? = null
    private lateinit var storageRef: StorageReference
    private lateinit var databaseReference: DatabaseReference
    private var mUploadTask: StorageTask<*>? = null
    private lateinit var prefs: SharedPreferences

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        validateAuth()
        storageRef = FirebaseStorage.getInstance().getReference("tours")
        databaseReference = FirebaseDatabase.getInstance().getReference("tours")
        btn_choose_img?.setOnClickListener {
            openFileChooser()
        }

        addTourBtn?.setOnClickListener {
            if (mUploadTask != null && mUploadTask!!.isInProgress) {
                Snackbar.make(view, "Загружается!...", 2000).show()
            } else {
                if (input_tour_name.text.isEmpty() || input_tour_desc.text.isEmpty()
                    || input_tour_city.text.isEmpty() || input_tour_country.text.isEmpty()
                    || input_tour_price.text.isEmpty() || input_tour_path.text.isEmpty()
                    || input_tour_players.text.isEmpty()
                ) {
                    Snackbar.make(view, "Не все поля заполнены!", 2000).show()

                } else {
                    when {
                        (input_flight_date.text.toString()
                            .trim() + " - " + input_arrival.text.toString()
                            .trim()) == "Туда - Обратно" -> {
                            Snackbar.make(view, "Период не указан!", 2000).show()
                        }
                        img_view.drawable == null -> {
                            Snackbar.make(view, "Картинка не выбрана!", 2000).show()
                        }
                        else -> {
                            uploadTour()
                        }
                    }
                }
            }
        }

        showAdds?.setOnClickListener {
            openUserToursFragment()
        }

        input_flight_date.setOnClickListener {
            val c = Calendar.getInstance()
            var day = c.get(Calendar.DAY_OF_MONTH)
            var month = c.get(Calendar.MONTH)
            var year = c.get(Calendar.YEAR)

            val dpd = DatePickerDialog(
                requireActivity(),
                android.R.style.Theme_Material_Light_Dialog,
                { _, selectedYear, monthOfYear, dayOfMonth ->
                    day = dayOfMonth
                    month = monthOfYear + 1
                    year = selectedYear
                    input_flight_date.text = "$day.$month.$year"

                }, year, month, day
            )
            dpd.show()
        }

        input_arrival.setOnClickListener {
            val c = Calendar.getInstance()
            var day = c.get(Calendar.DAY_OF_MONTH)
            var month = c.get(Calendar.MONTH)
            var year = c.get(Calendar.YEAR)

            val dpd1 = DatePickerDialog(
                requireActivity(),
                android.R.style.Theme_Material_Light_Dialog,
                { _, selectedYear, monthOfYear, dayOfMonth ->
                    day = dayOfMonth
                    month = monthOfYear + 1
                    year = selectedYear
                    input_arrival.text = "$day.$month.$year"

                }, year, month, day
            )
            dpd1.show()
        }
    }

    private fun openUserToursFragment() {
        scrollViewLayout?.visibility = View.GONE
        warningLayout?.visibility = View.GONE
        view?.findNavController()
            ?.navigate(R.id.action_nav_create_tour_to_nav_userTours)
    }

    private fun validateAuth() {
        prefs = PreferenceManager.getDefaultSharedPreferences(requireContext())
        val prefsUserId = prefs.getString("googleUserID", "").orEmpty()
        if (prefsUserId.isEmpty()) {
            scrollViewLayout?.visibility = View.GONE
            warningLayout?.visibility = View.VISIBLE
        } else {
            scrollViewLayout?.visibility = View.VISIBLE
            warningLayout?.visibility = View.GONE
        }
    }

    private fun getFileExtension(uri: Uri): String {
        val cR: ContentResolver? = context?.contentResolver
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(cR?.getType(uri))!!
    }

    private fun uploadTour() {
        if (mImgURi != null) {
            mImgURi?.let { uri ->
                prefs = PreferenceManager.getDefaultSharedPreferences(requireContext())
                val prefsUserId = prefs.getString("googleUserID", "").orEmpty()
                val fileReference = storageRef.child(
                    System.currentTimeMillis().toString() + "." + getFileExtension(uri)
                )
                mUploadTask = fileReference.putFile(uri)
                    .addOnSuccessListener {
                        fileReference.downloadUrl.addOnSuccessListener {
                            val handler = Handler(Looper.getMainLooper())
                            handler.postDelayed(
                                {
                                    progress_bar?.progress = 0
                                }, 5000
                            )
                            view?.let { it1 ->
                                Snackbar.make(it1, "Успешно добавлен!", 2500).show()
                            }

                            val newTour = NewTourItem(
                                id = prefsUserId,
                                name = input_tour_name?.text.toString().trim(),
                                imageUrl = it.toString(),
                                city = input_tour_city?.text.toString().trim(),
                                country = input_tour_country?.text.toString().trim(),
                                desc = input_tour_desc?.text.toString().trim(),
                                path = input_tour_path?.text.toString().trim(),
                                period = input_flight_date.text.toString()
                                    .trim() + " - " + input_arrival.text.toString().trim(),
                                price = input_tour_price?.text.toString().trim().toInt(),
                                players = input_tour_players?.text.toString().trim().toInt()
                            )
                            val uploadId = databaseReference.push().key.toString()
                            databaseReference.child(uploadId).setValue(newTour)
                        }

                    }

                    .addOnFailureListener { e ->
                        Toast.makeText(requireContext(), e.message, Toast.LENGTH_SHORT).show()
                    }
                    .addOnProgressListener { taskSnapshot ->
                        val progress =
                            (100.0 * taskSnapshot.bytesTransferred / taskSnapshot.totalByteCount)
                        progress_bar?.progress = progress.toInt()
                    }
            }
        } else {
            view?.let { Snackbar.make(it, "Файл не выбран!", 2000).show() }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMG_REQUEST && resultCode == RESULT_OK && data != null && data.data != null) {
            mImgURi = data.data!!
            Picasso.get().load(mImgURi).into(this.img_view)
        }
    }

    private fun openFileChooser() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, PICK_IMG_REQUEST)
    }
}