package kz.jumysbar.intelteam.ui.tour.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_created_tour.shareWithTour
import kotlinx.android.synthetic.main.fragment_created_tour.output_city
import kotlinx.android.synthetic.main.fragment_created_tour.output_country
import kotlinx.android.synthetic.main.fragment_created_tour.output_description
import kotlinx.android.synthetic.main.fragment_created_tour.output_tour_name
import kotlinx.android.synthetic.main.fragment_created_tour.tourDate
import kotlinx.android.synthetic.main.fragment_created_tour.output_route
import kotlinx.android.synthetic.main.fragment_created_tour.output_price
import kotlinx.android.synthetic.main.fragment_created_tour.imgViewTour
import kz.jumysbar.intelteam.R
import kz.jumysbar.intelteam.ui.tour.model.NewTourItem

class TourDetailFragment : Fragment(R.layout.fragment_created_tour) {
    private var databaseRef: DatabaseReference? = null
    private var mDBListener: ValueEventListener? = null
    private var storage: FirebaseStorage? = null
    private var compositeDisposable = CompositeDisposable()
    private var mKey: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val share_text_1_btn: View = shareWithTour
        share_text_1_btn.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.putExtra(Intent.EXTRA_TEXT, "Крутейшее приложение для туров:")
            intent.type = "text/plain"
            startActivity(Intent.createChooser(intent, "Поделиться с:"))
        }
        val prefs = PreferenceManager.getDefaultSharedPreferences(requireContext())
        mKey = prefs.getString("tourKey", "")
        if (mKey != null) {
            val disposable = Completable.fromAction {
                storage = FirebaseStorage.getInstance()
                databaseRef = mKey?.let {
                    FirebaseDatabase.getInstance().getReference("tours")
                        .child(it)
                }
                mDBListener = databaseRef?.addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {

                        val item = snapshot.getValue(NewTourItem::class.java)
                        item?.let {
                            output_city?.text = it.tourCity + ", "
                            output_country?.text = it.tourCountry
                            output_description?.text = it.tourDesc
                            output_tour_name?.text = it.tourName
                            tourDate?.text = it.tourPeriod
                            output_route?.text = it.tourPath
                            output_price?.text = it.tourPrice.toString() + " kzt"

                            imgViewTour?.let { v ->
                                Picasso.get().load(it.tourImgUrl)
                                    .fit().centerCrop().placeholder(R.mipmap.ic_launcher)
                                    .into(v)
                            }
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Log.e("Error", "onCancelled: $error.message")
                    }
                })
            }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        Log.d("App", "Successfull")
                    },
                    {
                        Log.e("App", "Error onAttach: ", it)
                    }
                )
            compositeDisposable.add(disposable)
        }
    }
}

