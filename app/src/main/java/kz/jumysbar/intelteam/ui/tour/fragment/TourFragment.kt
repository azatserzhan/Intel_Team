package kz.jumysbar.intelteam.ui.tour.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.*
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_tour.toolbarTours
import kotlinx.android.synthetic.main.fragment_tour.swipeTourFragment
import kotlinx.android.synthetic.main.user_tours_fragment.*
import kotlinx.android.synthetic.main.user_tours_fragment.progress_circle
import kotlinx.android.synthetic.main.user_tours_fragment.user_recyclerView
import kz.jumysbar.intelteam.R
import kz.jumysbar.intelteam.ui.tour.adapter.UserTourAdapter
import kz.jumysbar.intelteam.ui.tour.model.NewTourItem
import kz.jumysbar.intelteam.ui.tour.util.Utils
import java.util.Collections

class TourFragment : Fragment(R.layout.fragment_tour) {
    private val tours = mutableListOf<NewTourItem>()
    private lateinit var tourLayoutManager: LinearLayoutManager
    private var tourAdapter: UserTourAdapter? = null
    private var databaseRef: DatabaseReference? = null
    private var mDBListener: ValueEventListener? = null
    private var compositeDisposable = CompositeDisposable()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        tourLayoutManager = GridLayoutManager(
            activity, 2
        )
        fetchData()
        refresh()
    }

    override fun onDestroy() {
        mDBListener?.let { databaseRef?.removeEventListener(it) }
        compositeDisposable.dispose()
        super.onDestroy()
    }

    private fun setupToolbar() {
        toolbarTours?.inflateMenu(R.menu.toolbar_tour_menu)
        toolbarTours?.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.sortAscPrice -> {
                    Collections.sort(tours, NewTourItem.ASC_TOURS_PRICE)
                    tourAdapter = UserTourAdapter(requireContext(), tours)
                    user_recyclerView?.apply {
                        layoutManager = tourLayoutManager
                        adapter = tourAdapter
                    }
                    tourAdapter?.notifyDataSetChanged()
                    return@setOnMenuItemClickListener true
                }
                R.id.sortDescPrice -> {
                    Collections.sort(tours, NewTourItem.DESC_TOURS_PRICE)
                    tourAdapter = UserTourAdapter(requireContext(), tours)
                    user_recyclerView?.apply {
                        layoutManager = tourLayoutManager
                        adapter = tourAdapter
                    }
                    tourAdapter?.notifyDataSetChanged()
                    return@setOnMenuItemClickListener true
                }
            }
            true
        }
    }

    private fun fetchData() {
        tourAdapter = UserTourAdapter(requireContext(), tours)
        user_recyclerView?.apply {
            layoutManager = tourLayoutManager
            adapter = tourAdapter
        }
      if(Utils.isNetworkAvailable(requireContext())){
          val disposable = Completable.fromAction {
              databaseRef = FirebaseDatabase.getInstance().getReference("tours")
              mDBListener = databaseRef?.addValueEventListener(object : ValueEventListener {
                  override fun onDataChange(snapshot: DataSnapshot) {
                      for (postSnapshot: DataSnapshot in snapshot.children) {
                          val item = postSnapshot.getValue(NewTourItem::class.java)
                          item?.mKey = postSnapshot.key
                          item?.let { tours.add(it) }
                          tourAdapter?.notifyDataSetChanged()
                          progress_circle?.visibility = View.INVISIBLE
                      }
                  }
                  override fun onCancelled(error: DatabaseError) {
                      Log.e("Error", "onCancelled: $error.message")
                      progress_circle?.visibility = View.INVISIBLE
                  }
              })
          }.subscribeOn(Schedulers.io())
              .observeOn(AndroidSchedulers.mainThread())
              .subscribe(
                  {
                      Log.d("App", "fetch tours in TourFr OK")
                  },
                  {
                      Log.e("App", "Error onAttach: ", it)
                  }
              )
          compositeDisposable.add(disposable)
      }else{
          progress_circle?.visibility = View.INVISIBLE
          view?.let { Snackbar.make(it, "Нет подключения к интернету!", 2500).show() }
      }

    }

    private fun refresh() {
        swipeTourFragment?.setOnRefreshListener {
            tours.clear()
            Handler(Looper.getMainLooper()).postDelayed(
                {
                    if (Utils.isNetworkAvailable(requireContext())) {
                        swipeTourFragment?.isRefreshing = false
                        fetchData()
                    } else {
                        swipeTourFragment?.isRefreshing = false
                        view?.let { Snackbar.make(it, "Нет подключения к интернету!", 2500).show() }
                    }
                }, 1500
            )
        }
    }
}