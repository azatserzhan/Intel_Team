package kz.jumysbar.intelteam.ui.tour.fragment

import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_tour.*
import kotlinx.android.synthetic.main.user_tours_fragment.swipeUserToursFragment
import kotlinx.android.synthetic.main.user_tours_fragment.progress_circle
import kotlinx.android.synthetic.main.user_tours_fragment.user_recyclerView
import kz.jumysbar.intelteam.R
import kz.jumysbar.intelteam.ui.tour.adapter.UserTourAdapter
import kz.jumysbar.intelteam.ui.tour.model.NewTourItem
import kz.jumysbar.intelteam.ui.tour.util.Utils

class UserToursFragment : Fragment(R.layout.user_tours_fragment),
    UserTourAdapter.itemClickListener {
    private var compositeDisposable = CompositeDisposable()
    private val toursList = mutableListOf<NewTourItem>()
    private lateinit var tourLayoutManager: LinearLayoutManager
    private var tourAdapter: UserTourAdapter? = null
    private var databaseRef: DatabaseReference? = null
    private var mDBListener: ValueEventListener? = null
    private var storage: FirebaseStorage? = null
    private lateinit var prefs: SharedPreferences

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tourLayoutManager = GridLayoutManager(activity, 2)
        fetchData()
        refresh()
    }

    private fun fetchData() {
        tourAdapter = UserTourAdapter(requireContext(), toursList)
        user_recyclerView?.apply {
            layoutManager = tourLayoutManager
            adapter = tourAdapter
        }
        tourAdapter?.setOnItemClickListener(this@UserToursFragment)
    if(Utils.isNetworkAvailable(requireContext())){
        val disposable = Completable.fromAction {
            storage = FirebaseStorage.getInstance()
            databaseRef = FirebaseDatabase.getInstance().getReference("tours")
            mDBListener = databaseRef?.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    toursList.clear()
                    prefs = PreferenceManager.getDefaultSharedPreferences(requireContext())
                    val prefsUserId = prefs.getString("googleUserID", "").orEmpty()
                    for (postSnapshot: DataSnapshot in snapshot.children) {
                        val item = postSnapshot.getValue(NewTourItem::class.java)
                        item?.mKey = postSnapshot.key
                        if (item?.userId == prefsUserId) {
                            item.let { toursList.add(it) }
                        }
                    }
                    tourAdapter?.notifyDataSetChanged()
                    progress_circle?.visibility = View.INVISIBLE
                }

                override fun onCancelled(error: DatabaseError) {
                    Log.e("Error", "onCancelled: $error.message")
                    progress_circle?.visibility = View.INVISIBLE
                }
            })
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    Log.d("App", "success fetch data!")
                },
                {
                    Log.e("App", "Error onAttach: ", it)
                }
            )
        compositeDisposable.add(disposable)
    }else{
        progress_circle?.visibility = View.INVISIBLE
     view?.let { Snackbar.make(it, "Нет подключения к интернету!", 2500).show() }
    }
    }

    private fun refresh() {
        swipeUserToursFragment?.setOnRefreshListener {
            toursList.clear()
            Handler(Looper.getMainLooper()).postDelayed(
                {
                    if (Utils.isNetworkAvailable(requireContext())) {
                        swipeUserToursFragment?.isRefreshing = false
                        fetchData()
                    } else {
                        swipeUserToursFragment?.isRefreshing = false
                        view?.let { Snackbar.make(it, "Нет подключения к интернету!", 2500).show() }
                    }
                }, 1500
            )
        }
    }

    override fun onDeleteClick(pos: Int) {
        val selectedItem = toursList[pos]
        val selectedKey = selectedItem.mKey
        val imageRef: StorageReference? = selectedItem.tourImgUrl?.let {
            storage?.getReferenceFromUrl(it)
        }
        imageRef?.delete()?.addOnSuccessListener {
            selectedKey?.let { key -> databaseRef?.child(key)?.removeValue() }
            view?.let { Snackbar.make(it, "Тур удален!", 3000).show() }

        }
    }

    override fun onDestroy() {
        mDBListener?.let { databaseRef?.removeEventListener(it) }
        super.onDestroy()
    }
}