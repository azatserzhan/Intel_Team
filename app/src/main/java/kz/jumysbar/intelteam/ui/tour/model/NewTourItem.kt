package kz.jumysbar.intelteam.ui.tour.model


class NewTourItem {
    var userId: String? = null
    var tourName: String? = null
    var tourImgUrl: String? = null
    var tourCity: String? = null
    var tourCountry: String? = null
    var tourDesc: String? = null
    var tourPath: String? = null
    var tourPeriod: String? = null
    var tourPrice: Int? = 0
    var tourPlayers: Int? = null
    var mKey: String? = null

    constructor()

    constructor(
        id: String,
        name: String,
        imageUrl: String,
        city: String,
        country: String,
        desc: String,
        path: String,
        period: String,
        price: Int,
        players: Int
    ) {
        userId = id
        var name = name
        if (name.trim { it <= ' ' } == "") {
            name = "No Name"
        }

        tourName = name
        tourImgUrl = imageUrl
        tourDesc = desc
        tourCity = city
        tourCountry = country
        tourPath = path
        tourPeriod = period
        tourPrice = price
        tourPlayers = players

    }

    companion object {
        val ASC_TOURS_PRICE: Comparator<NewTourItem> =
            Comparator { o1, o2 ->
                return@Comparator o2.tourPrice?.let { o1.tourPrice?.compareTo(it) }!!
            }
        val DESC_TOURS_PRICE: Comparator<NewTourItem> =
            Comparator { o1, o2 ->
                return@Comparator o1.tourPrice?.let { o2.tourPrice?.compareTo(it) }!!
            }
    }
}
