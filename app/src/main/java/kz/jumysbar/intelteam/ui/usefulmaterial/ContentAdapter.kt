package kz.jumysbar.intelteam.ui.usefulmaterial

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_materials.view.code_name
import kotlinx.android.synthetic.main.row_materials.view.content_materials
import kotlinx.android.synthetic.main.row_materials.view.linearlayout
import kotlinx.android.synthetic.main.row_materials.view.expandable_layout
import kz.jumysbar.intelteam.R

class ContentAdapter(val contentList: List<ContentForMaterials>) :
    RecyclerView.Adapter<ContentAdapter.ContentViewHolder>() {
    class ContentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var codeNameTxt: TextView = itemView.code_name
        var contentTxt: TextView = itemView.content_materials
        var linearLayout: LinearLayout = itemView.linearlayout
        var expandable_layout: RelativeLayout = itemView.expandable_layout

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.row_materials, parent, false)
        return ContentViewHolder(view)
    }

    override fun onBindViewHolder(holder: ContentViewHolder, position: Int) {
        val contents: ContentForMaterials = contentList[position]
        holder.codeNameTxt.text = contents.code_name
        holder.contentTxt.text = contents.content_materials

        val isExpandable: Boolean = contentList[position].expandable
        holder.expandable_layout.visibility = if (isExpandable) View.VISIBLE else View.GONE

        holder.linearLayout.setOnClickListener {
            val cnt = contentList[position]
            cnt.expandable = !cnt.expandable
            notifyItemChanged(position)
        }
    }

    override fun getItemCount(): Int {
        return contentList.size
    }
}