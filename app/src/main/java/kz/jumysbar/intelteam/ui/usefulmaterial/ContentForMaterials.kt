package kz.jumysbar.intelteam.ui.usefulmaterial

class ContentForMaterials(
    val code_name: String,
    val content_materials: String,
    var expandable: Boolean = false
) {}